import sqlite3
def new(name):  
    con = sqlite3.connect(name)
    cur = con.cursor()

    cur.execute("""CREATE TABLE spam
                (spam_start TEXT,
                spam_message TEXT,
                spam_filter TEXT,
                spam_end TEXT)""")
                
    cur.execute("""CREATE TABLE client
                (client_phone_num INTEGER,
                client_operator_code INTEGER,
                client_tag TEXT,
                client_timezone INTEGER)""")

    cur.execute("""CREATE TABLE sms
                (sms_create_date TEXT, 
                sms_status TEXT,
                sms_client INTEGER,
                sms_spam INTEGER,
                FOREIGN KEY(sms_client) REFERENCES client(rowid),
                FOREIGN KEY(sms_spam) REFERENCES spam(rowid))""")
