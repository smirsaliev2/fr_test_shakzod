import requests
import sqlite3
import threading
from datetime import datetime, timedelta
import random
import create_db
import os
from decouple import config


token = config('token', default='')
headers = {"Authorization": token}

class Spam():
    def __init__():
        if 'sms_spam.db' not in os.listdir(os.getcwd()):
            '''Создаем БД, если ее нет в директории.'''
            create_db.new('sms_spam.db')
        con = sqlite3.connect('sms_spam.db')
        cur = con.cursor()
    def datetime_to_str(dt):
        '''Возвращает строку формата dd/mm/yy HH:MM:SS от объекта datetime.'''
        date_time = dt.strftime('%d/%m/%Y, %H:%M:%S')
        return date_time
    def str_to_datetime(dt_string):
        '''Возвращает объект datetime от полученной строки.'''
        date_time = datetime.strptime(dt_string, '%d/%m/%Y, %H:%M:%S')
        return date_time

    class client():
        def add(phone, operator_code, tag, timezone):
            '''Добавляет данные клиента в БД.'''
            args = (phone, operator_code, tag, timezone)
            con = sqlite3.connect('sms_spam.db')
            cur = con.cursor()
            cur.execute('INSERT INTO client VALUES (?, ?, ?, ?)', args)
            con.commit()
        def edit(client_phone_num, column, value):
            '''Изменить данные о клиенте по номеру телефона.
            :param column: имя столбца с БД.'''
            string = f'UPDATE client SET {column} = ? WHERE client_phone_num=?'
            con = sqlite3.connect('sms_spam.db')
            cur = con.cursor()
            cur.execute(string, (value, client_phone_num))
            con.commit()
        def delete(client_column, value):
            '''Удаляет строку клиента по имени колонки в таблице БД.'''
            query = f'DELETE FROM client WHERE {client_column}= ?'
            con = sqlite3.connect('sms_spam.db')
            cur = con.cursor()
            a = cur.execute(query, (value,))
            con.commit()

    class sms():
        def delete(spam_rowid):
            '''Удалаяет неисполненные смс по rowid спама.'''
            con = sqlite3.connect('sms_spam.db')
            cur = con.cursor()
            cur.execute('DELETE FROM sms WHERE sms_spam=? AND \
                sms_status IS "0"', (spam_rowid, ))

    class spam():
        def create_sms(self, spam_rowid, client_filter, filter_value):
            '''Создание строк смс в БД при создании нового спама.'''
            #Список клиентов, сформированный из запроса в БД по фильтру.
            query = f'SELECT rowid, * FROM client WHERE {client_filter}=?' 
            self.cur.execute(query, (filter_value,))
            filtered_clients = self.cur.fetchall()
            self.sms_stats['created'] = len(filtered_clients)
            #Создаем смски для клиентов.
            for client in filtered_clients:
                self.cur.execute('INSERT INTO sms VALUES (?, ?, ?, ?)', (
                    '', #sms_create_date
                    '0', #sms_status
                    client[0], #rowid клиента.
                    spam_rowid
                ))
            self.con.commit()
        def run(self):
            #Проходимся по списку и отправляем запрос на отправку смс.
            for row in self.to_be_delivered:
                body = {
                "id": row[0],
                "phone": row[1],
                "text": row[2]}
                url = f'https://probe.fbrq.cloud/v1/send/{row[0]}'
                req = requests.post(url, headers=headers, json=body)
                #При успешном запросе обновляем дату создания смс и статус.
                if req.status_code == 200:
                    #Datetime из строки ответа сервера.
                    dt = datetime.strptime(req.headers['Date'], 
                        '%a, %d %b %Y %H:%M:%S %Z') 
                    #Из datetime в строку нужного формата.
                    dt_str = Spam.datetime_to_str(dt)
                    self.cur.execute('''UPDATE sms SET sms_create_date = ?, sms_status = ? WHERE rowid = ?''', (dt_str, 'OK', row[0]))
                    self.con.commit()
                    print(f'sms with {row[0]} id was sent')
                    self.sms_stats['delivered'] += 1
                #Если запрос на отправку не был успешен.
                else:
                    self.sms_stats['failed'] += row[0]
                    
        def __init__(self, spam_start, message, filter, filter_value, 
                     spam_end):
            '''Создаем экземпляр спама, добавляем данные в БД и создаем смс.
            :param spam_start: дата в формате '%d/%m/%Y, %H:%M:%S'
            :param spam_end: дата в формате "%d/%m/%Y, %H:%M:%S"'''
            self.con = sqlite3.connect('sms_spam.db', check_same_thread=False)
            self.cur = self.con.cursor()
            #Добавляем данные спама в БД.
            self.cur.execute('INSERT INTO spam VALUES (?, ?, ?, ?)', (spam_start, 
                         message, filter_value, spam_end))
            self.con.commit()
            #Создаем экземпляр спама.
            self.rowid = self.cur.lastrowid #Получаем rowid из курсора.
            self.start = Spam.str_to_datetime(spam_start)
            self.end = Spam.str_to_datetime(spam_end)
            self.message = message
            self.filter = filter
            self.filter_value = filter_value
            self.failed_sms_id = []
            self.sms_stats = {'created': 0, 'failed': set(), 'delivered': 0}
            #Создаем смски.
            self.create_sms(self.rowid, self.filter, self.filter_value)
            #Сводная таблица смс, спам, клиент по фильтру из спама.
            self.cur.execute("""SELECT sms.rowid, client_phone_num, spam_message
                FROM spam
                JOIN sms ON spam.rowid = sms.sms_spam
                JOIN client ON client.rowid = sms.sms_client
                WHERE sms_spam = ?
                AND sms_status IS NOT 'OK'""", (self.rowid,)) 
            self.to_be_delivered = self.cur.fetchall() #Сохраняем в экземпляре.
            self.all_sms_list = self.to_be_delivered.copy()
            #Запускаем спам/ставим делей на запуск пока есть неотправленные смс.
            if datetime.now() < self.start:
                start_diff = (self.start - datetime.now()).seconds
                #threading.Timer запустит self.run через start_diff секунд.
                timer = threading.Timer(start_diff, self.run)
                timer.start()
            else:
                self.run()
        def __setattr__(self, attr, value):
            self.__dict__[attr] = value
            #Обновляем БД, если происходит изменение названий колонок спама.
            if attr in ['start', 'end', 'message', 'filter']:
                a = 'spam_' + attr
                string = f'UPDATE spam SET {a} = ? WHERE rowid=?'
                self.cur.execute(string, (value, self.rowid))
                self.con.commit()
        def get_stats(self):
            return self.sms_stats
        def __del__(self, from_bd=False):
            '''Удалить экземпляр рассылки.'''
            if from_bd == True:
                a = self.cur.execute('DELETE FROM spam WHERE rowid= ?', (self.rowid,))
                self.con.commit()


def add_random_clients(n):
        '''Добавляет n рандомных клиентов в БД.'''
        for i in range(n):
            phone = random.randint(79000000000, 79999999999)
            code = int(str(phone)[1:4])
            tag = random.choice(('kazan', 'moscow', 'vladivostok'))
            timezone = random.randint(0, 24)
            Spam.client.add(phone, code, tag, timezone)
     
def main():
    pass

    
def show_db():
    con = sqlite3.connect('sms_spam.db')
    cur = con.cursor()
    print('SPAM')
    for i in cur.execute('SELECT rowid, * FROM spam'):
        print(i)
    names = [description[0] for description in cur.description]
    print(names)

    print('CLIENT')
    for i in cur.execute('SELECT rowid, * FROM client'):
        print(i)
    names = [description[0] for description in cur.description]
    print(names)

    print('SMS')
    for i in cur.execute('SELECT rowid, * FROM sms'):
        print(i)
    names = [description[0] for description in cur.description]
    print(names)


if __name__ == '__main__':
    main()
    show_db()
